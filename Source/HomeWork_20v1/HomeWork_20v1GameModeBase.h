// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HomeWork_20v1GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK_20V1_API AHomeWork_20v1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

 // Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
//#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.5f;
	SlowSpeed = 1.f;
	NitroSpeed = 0.25f;
	Timer = 10.f;
	BeginTimer = Timer;
	GoSlow = false;
	GoNitro = false;
	BeginMovementSpeed = MovementSpeed;
	LastMoveDirection = EMovementDerection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(5);
	SetActorTickInterval(MovementSpeed);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	MoveSlowDown();
	MoveNitroSpeed();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	/*SnakeElements.Num()* ElementSize;*/
	//FVector NewLocation = FVector(SnakeElements.Num() * ElementSize, 0, 0)
	//FTransform NewTransform = FTransform(GetActorLocation() - NewLocation);
	for (int i = 0;i < ElementsNum;++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	//if (LastMoveDirection == EMovementDerection::UP)
	//{
	//	MovementVector = FVector(MovementSpeedDelta,0,0);
	//}
	switch (LastMoveDirection)
	{
	case EMovementDerection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDerection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDerection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDerection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	//AddActorWorldOffset(MovementVector);

	for (int i = SnakeElements.Num() - 1;i > 0;i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement -> SetActorLocation(PrevLocation);
	}
	SnakeElements[0]-> AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverLappedElement, AActor* Other)
{
	if (IsValid(OverLappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverLappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this,bIsFirst);
		}
	}
}

void ASnakeBase::MoveSlowDown()
{
	if (GoSlow == true)
	{
		Timer -= BeginMovementSpeed;
		if (Timer > 0)
		{
			/*MovementSpeed = 1.f;*/
			SetActorTickInterval(SlowSpeed);
		}
		else
		{
			MovementSpeed = BeginMovementSpeed;
			SetActorTickInterval(MovementSpeed);
			Timer = BeginTimer;
			GoSlow = false;
		}
	}
}

void ASnakeBase::MoveNitroSpeed()
{
	if (GoNitro == true)
	{
		Timer -= BeginMovementSpeed;
		if (Timer > 0)
		{
			//MovementSpeed = 0.25f;
			SetActorTickInterval(NitroSpeed);
		}
		else
		{
			MovementSpeed = BeginMovementSpeed;
			SetActorTickInterval(MovementSpeed);
			Timer = BeginTimer;
			GoNitro = false;
		}
	}
}

  
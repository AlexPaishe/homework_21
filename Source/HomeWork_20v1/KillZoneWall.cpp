// Fill out your copyright notice in the Description page of Project Settings.


#include "KillZoneWall.h"
#include "SnakeBase.h"

// Sets default values
AKillZoneWall::AKillZoneWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AKillZoneWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKillZoneWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKillZoneWall::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}


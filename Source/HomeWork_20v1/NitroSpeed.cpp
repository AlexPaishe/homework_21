// Fill out your copyright notice in the Description page of Project Settings.


#include "NitroSpeed.h"
#include "SnakeBase.h"

// Sets default values
ANitroSpeed::ANitroSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANitroSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANitroSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ANitroSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Timer = Snake->BeginTimer * 2;
			Snake->GoSlow = false;
			Snake->GoNitro = true;
			Snake->MoveSlowDown();
			this->Destroy();
		}
	}
}

